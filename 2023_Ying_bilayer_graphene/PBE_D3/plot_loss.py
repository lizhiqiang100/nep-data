from pylab import *

##set figure properties
aw = 1.5
fs = 14
lw = 1.5
font = {'size'   : fs}
matplotlib.rc('font', **font)
matplotlib.rc('axes' , lw=aw)

def set_fig_properties(ax_list):
    tl = 6
    tw = 1.5
    tlm = 3
    
    for ax in ax_list:
        ax.tick_params(which='major', length=tl, width=tw)
        ax.tick_params(which='minor', length=tlm, width=tw)
        ax.tick_params(which='both', axis='both', direction='out', right=False, top=False)


figure(figsize=(15, 8))
loss_6 = loadtxt('6_4.5/loss.out')
loss_6[:,0] = np.arange(1, len(loss_6) + 1)*100
loss_8 = loadtxt('8_4.5/loss.out')
loss_8[:,0] = np.arange(1, len(loss_8) + 1)*100
loss_10 = loadtxt('10_4.5/loss.out')
loss_10[:,0] = np.arange(1, len(loss_10) + 1)*100

subplot(1, 3, 1)
set_fig_properties([gca()])
loglog(loss_6[:, 0],  loss_6[:, 4],  ls="-", lw=lw, c = "C0", label="6, 4.5")
loglog(loss_8[:, 0],  loss_8[:, 4],  ls="-", lw=lw, c = "C1", label="8, 4.5")
loglog(loss_10[:, 0], loss_10[:, 4], ls="-", lw=lw, c = "C2", label="10, 4.5")
xlim([1e2, 5e5])
# ylim([1e-3, 1e-1])
xlabel('Generation')
ylabel('Loss')
legend(loc="lower left")
title("(a) Energy")

subplot(1, 3, 2)
set_fig_properties([gca()])
loglog(loss_6[:, 0],  loss_6[:, 5],  ls="-", lw=lw, c = "C0", label="6, 4.5")
loglog(loss_8[:, 0],  loss_8[:, 5],  ls="-", lw=lw, c = "C1", label="8, 4.5")
loglog(loss_10[:, 0], loss_10[:, 5], ls="-", lw=lw, c = "C2", label="10, 4.5")
xlim([1e2, 5e5])
# ylim([5e-2, 5e-1])
xlabel('Generation')
title("(b) Force")

subplot(1, 3, 3)
set_fig_properties([gca()])
loglog(loss_6[:, 0],  loss_6[:, 6],  ls="-", lw=lw, c = "C0", label="6, 4.5")
loglog(loss_8[:, 0],  loss_8[:, 6],  ls="-", lw=lw, c = "C1", label="8, 4.5")
loglog(loss_10[:, 0], loss_10[:, 6], ls="-", lw=lw, c = "C2", label="10, 4.5")
xlim([1e2, 5e5])
# ylim([1e-2, 1e0])
xlabel('Generation')
title("(c) Virial")

subplots_adjust(wspace=0.3, hspace=0.3)
savefig("loss.png", bbox_inches='tight')
