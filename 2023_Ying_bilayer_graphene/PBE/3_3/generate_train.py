# -*- coding: utf-8 -*-
"""
Created on Tue Aug 29 22:01:56 2023

@author: hityingph
"""

from ase.io import read, write
import random


reference = read("PBE_reference.xyz", ":")
small = [r for r in reference if len(r)==4]
large = [r for r in reference if len(r)==64]
large_test = random.sample(range(len(large)), 100)
write("test.xyz", [large[i] for i in range(len(large)) if i in large_test])
write("train.xyz", small + [large[i] for i in range(len(large)) if i not in large_test])
