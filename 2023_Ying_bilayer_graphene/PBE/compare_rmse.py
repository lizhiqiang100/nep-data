from pylab import *

##set figure properties
aw = 1.5
fs = 15
lw = 1.5
ms = 10
font = {'size'   : fs}
matplotlib.rc('font', **font)
matplotlib.rc('axes' , lw=aw)

def set_fig_properties(ax_list):
    tl = 6
    tw = 1.5
    tlm = 3
    
    for ax in ax_list:
        ax.tick_params(which='major', length=tl, width=tw)
        ax.tick_params(which='minor', length=tlm, width=tw)
        ax.tick_params(which='both', axis='both', direction='out', right=False, top=False)


rs = [3, 3.5, 4, 4.5, 5]
rmses = np.zeros((len(rs), 4))
for i, r in enumerate(rs):
    loss = np.loadtxt(f"{r}_{r}/loss.out")
    loss = loss[-1]
    rmses[i, 0] = r
    rmses[i, 1] = loss[4] * 1000 # energy rmse in meV/atom
    rmses[i, 2] = loss[5] * 1000 # force rmse in meV/angstrom
    rmses[i, 3] = loss[6] * 1000 # virial rmse in meV/atom

figure(figsize=(8, 8))
subplot(3, 1, 1)
set_fig_properties([gca()])
plot(rmses[:, 0],  rmses[:, 1], 'o', c="C0", ms=ms)
ylabel('Energy RMSE \n(meV/atom)')
xlim([2.9, 5.1])
gca().set_xticks([])
ylim([-0.5, 10.5])
gca().set_yticks(linspace(0, 10, 6))
text(4, 9.4, "(a)", fontsize = fs + 2)

subplot(3, 1, 2)
set_fig_properties([gca()])
plot(rmses[:, 0],  rmses[:, 2], 's', c="C1", ms=ms)
ylabel("Force RMSE \n" + r"(meV/$\rm{\AA}$)")
xlim([2.9, 5.1])
gca().set_xticks([])
ylim([43, 72])
gca().set_yticks(linspace(45, 70, 6))
text(4, 69.1, "(b)", fontsize = fs + 2)

subplot(3, 1, 3)
set_fig_properties([gca()])
plot(rmses[:, 0],  rmses[:, 3], 'D', c="C2", ms=ms)
xlabel(r'Cutoff radius of NEP ($\rm{\AA}$)')
ylabel('Virial RMSE \n(meV/atom)')
xlim([2.9, 5.1])
gca().set_xticks(linspace(3, 5, 5))
ylim([10, 55])
gca().set_yticks(linspace(10, 50, 5))
text(4, 50.5, "(c)", fontsize = fs + 2)

subplots_adjust(hspace=0)
savefig("NEP_RMSE.pdf", bbox_inches='tight')
