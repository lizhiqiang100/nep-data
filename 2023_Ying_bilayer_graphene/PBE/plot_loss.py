from pylab import *

##set figure properties
aw = 1.5
fs = 14
lw = 1.5
font = {'size'   : fs}
matplotlib.rc('font', **font)
matplotlib.rc('axes' , lw=aw)

def set_fig_properties(ax_list):
    tl = 6
    tw = 1.5
    tlm = 3
    
    for ax in ax_list:
        ax.tick_params(which='major', length=tl, width=tw)
        ax.tick_params(which='minor', length=tlm, width=tw)
        ax.tick_params(which='both', axis='both', direction='out', right=False, top=False)


figure(figsize=(15, 8))
loss_3_3 = loadtxt('3_3/loss.out')
loss_3_3[:,0] = np.arange(1, len(loss_3_3) + 1)*100
loss_35_35 = loadtxt('3.5_3.5/loss.out')
loss_35_35[:,0] = np.arange(1, len(loss_35_35) + 1)*100
loss_4_4 = loadtxt('4_4/loss.out')
loss_4_4[:,0] = np.arange(1, len(loss_4_4) + 1)*100
loss_45_45 = loadtxt('4.5_4.5/loss.out')
loss_45_45[:,0] = np.arange(1, len(loss_45_45) + 1)*100
loss_5_5 = loadtxt('5_5/loss.out')
loss_5_5[:,0] = np.arange(1, len(loss_5_5) + 1)*100

subplot(1, 3, 1)
set_fig_properties([gca()])
loglog(loss_3_3[:, 0],   loss_3_3[:, 4],    ls="-", lw=lw, c = "C0", label="3")
loglog(loss_35_35[:, 0], loss_35_35[:, 4],  ls="-", lw=lw, c = "C1", label="3.5")
loglog(loss_4_4[:, 0],   loss_4_4[:, 4],    ls="-", lw=lw, c = "C2", label="4")
loglog(loss_45_45[:, 0], loss_45_45[:, 4],  ls="-", lw=lw, c = "C3", label="4.5")
loglog(loss_5_5[:, 0],   loss_5_5[:, 4],    ls="-", lw=lw, c = "C4", label="5")
xlim([1e2, 5e5])
# ylim([1e-3, 1e-1])
xlabel('Generation')
ylabel('Loss')
legend(loc="lower left")
title("(a) Energy")

subplot(1, 3, 2)
set_fig_properties([gca()])
loglog(loss_3_3[:, 0],   loss_3_3[:, 5],    ls="-", lw=lw, c = "C0", label="3")
loglog(loss_35_35[:, 0], loss_35_35[:, 5],  ls="-", lw=lw, c = "C1", label="3.5")
loglog(loss_4_4[:, 0],   loss_4_4[:, 5],    ls="-", lw=lw, c = "C2", label="4")
loglog(loss_45_45[:, 0], loss_45_45[:, 5],  ls="-", lw=lw, c = "C3", label="4.5")
loglog(loss_5_5[:, 0],   loss_5_5[:, 5],    ls="-", lw=lw, c = "C4", label="5")
xlim([1e2, 5e5])
# ylim([5e-2, 5e-1])
xlabel('Generation')
title("(b) Force")

subplot(1, 3, 3)
set_fig_properties([gca()])
loglog(loss_3_3[:, 0],   loss_3_3[:, 6],    ls="-", lw=lw, c = "C0", label="3")
loglog(loss_35_35[:, 0], loss_35_35[:, 6],  ls="-", lw=lw, c = "C1", label="3.5")
loglog(loss_4_4[:, 0],   loss_4_4[:, 6],    ls="-", lw=lw, c = "C2", label="4")
loglog(loss_45_45[:, 0], loss_45_45[:, 6],  ls="-", lw=lw, c = "C3", label="4.5")
loglog(loss_5_5[:, 0],   loss_5_5[:, 6],    ls="-", lw=lw, c = "C4", label="5")
xlim([1e2, 5e5])
# ylim([1e-2, 1e0])
xlabel('Generation')
title("(c) Virial")

subplots_adjust(wspace=0.3, hspace=0.3)
savefig("loss.png", bbox_inches='tight')
