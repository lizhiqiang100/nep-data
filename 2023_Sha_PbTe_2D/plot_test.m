clear; close all;
load energy_test.out;load virial_test.out;load force_test.out; load loss.out; 

rmse_energy=sqrt(mean((energy_test(:,1)-energy_test(:,2)).^2))
force_diff=reshape(force_test(:,4:6)-force_test(:,1:3),size(force_test,1)*3,1);
rmse_force=sqrt(mean(force_diff.^2))
virial_test=virial_test(virial_test(:,2)>-1000,:);
rmse_virial=sqrt(mean((virial_test(:,1)-virial_test(:,2)).^2))

figure;
subplot(2,2,1);
loglog(loss(:,3:7),'-','linewidth',2); hold on;
xlabel('Generation/100','fontsize',15,'interpreter','latex');
ylabel('Loss functions','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
legend('L1-Reg','L2-Reg','Energy-test','Force-test','Virial-test');
axis tight

subplot(2,2,2);
plot(energy_test(:,2),energy_test(:,1),'.','markersize',20); hold on;
plot(-3.6:0.01:-3.3,-3.6:0.01:-3.3,'linewidth',2);
xlabel('DFT energy (eV/atom)','fontsize',15,'interpreter','latex');
ylabel('NEP energy (eV/atom)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_energy*1000,'%.1f'),' meV/atom']);
axis tight;

subplot(2,2,3);
plot(force_test(:,4:6),force_test(:,1:3),'.','markersize',20); hold on;
plot(-4:0.01:4,-4:0.01:4,'linewidth',2);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/$\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_force*1000,'%.0f'),' meV/A']);
axis tight;

subplot(2,2,4);
plot(virial_test(:,2),virial_test(:,1),'.','markersize',20); hold on;
plot(-1:0.01:1,-1:0.01:1,'linewidth',2);
xlabel('DFT virial (eV/atom)','fontsize',15,'interpreter','latex');
ylabel('NEP virial (eV/atom)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_virial*1000,'%.1f'),' meV/atom']);
axis tight;
