clear; close all;
load energy_train.out;load virial_train.out;load force_train.out; load loss.out; 

rmse_energy=sqrt(mean((energy_train(:,1)-energy_train(:,2)).^2))
force_diff=reshape(force_train(:,4:6)-force_train(:,1:3),size(force_train,1)*3,1);
rmse_force=sqrt(mean(force_diff.^2))
virial_train=virial_train(virial_train(:,2)~=0,:);
rmse_virial=sqrt(mean((virial_train(:,1)-virial_train(:,2)).^2))

figure;
subplot(2,2,1);
loglog(loss(:,3:7),'-','linewidth',2); hold on;
xlabel('Generation/100','fontsize',15,'interpreter','latex');
ylabel('Loss functions','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
legend('L1-Reg','L2-Reg','Energy-train','Force-train','Virial-trian');
axis tight

subplot(2,2,2);
plot(energy_train(:,2),energy_train(:,1),'.','markersize',20); hold on;
plot(-5:0.01:15,-5:0.01:15,'linewidth',2);
xlabel('DFT energy (eV/atom)','fontsize',15,'interpreter','latex');
ylabel('NEP energy (eV/atom)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_energy*1000,'%.0f'),' meV/atom']);
axis tight;

subplot(2,2,3);
plot(force_train(:,4:6),force_train(:,1:3),'.','markersize',20); hold on;
plot(-200:0.01:200,-200:0.01:200,'linewidth',2);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/$\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_force*1000,'%.0f'),' meV/A']);
axis tight;

subplot(2,2,4);
plot(virial_train(:,2),virial_train(:,1),'.','markersize',20); hold on;
plot(-7:0.01:12,-7:0.01:12,'linewidth',2);
xlabel('DFT virial (eV/atom)','fontsize',15,'interpreter','latex');
ylabel('NEP virial (eV/atom)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
title(['RMSE = ',num2str(rmse_virial*1000,'%.0f'),' meV/atom']);
axis tight;
